import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand text-dark" to="/">Shift into Gear</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <div className="dropdown">
              <button className="btn dropdown-toggle" data-bs-toggle="dropdown">Inventory</button>
                <ul className="dropdown-menu bg-dark ">
                  <li className="nav-item"><NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/inventory/automobiles">See inventory</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/inventory/models">See car models</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/inventory/manufacturers">See manufacturers</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/create/model">Add a new model</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/create/manufacturer">Add a new manufacturer</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/create/automobile">Add a auto info</NavLink></li>
                </ul>
            </div>
            <div className="dropdown">
              <button className="btn dropdown-toggle" data-bs-toggle="dropdown">Sales</button>
                <ul className="dropdown-menu bg-dark">
                  <li className="nav-item"><NavLink className="nav-link" to="/sales/employees">List of Employees</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/customers/list">List of Customers</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/customers/new">Add a potential customer</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/employees/new">Add a sales person</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/sales/new">Add a sales record</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/sales">See sales records</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/sales/record">Search for salesperson's records</NavLink></li>
                </ul>
            </div>
            <div className="dropdown">
              <button className="btn dropdown-toggle" data-bs-toggle="dropdown">Service</button>
                <ul className="dropdown-menu bg-dark">
                  <li className="nav-item"><NavLink className="nav-link" to="/technicians/new">Enter a Technician</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/technicians">List of Technicians</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/appointments/new">Make a appointment</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/appointments">List of appointments</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/appointments/history">History of appointments</NavLink></li>
                </ul>
              </div>
            </ul>
          </div>
        </div>
        </nav>
      )
    }

export default Nav;
