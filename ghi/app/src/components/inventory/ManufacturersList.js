import React, { useEffect, useState } from 'react';

function ManufacturersList () {
    const [manufacturers, setManufacturers] = useState([])

    const getData = async () => {
        const response = await fetch("http://localhost:8100/api/manufacturers/")
        if (response.ok) {
            const data = await response.json()
            setManufacturers(data.manufacturers)
        }
    }

    useEffect(() =>{
        getData()
    }, [])

    return (
      <>
        <h1> Manufacturers </h1>
          <tbody>
            {manufacturers.map(manufacturer => {
              return (
                <tr key={manufacturer.href}>
                  <div className="card" style={{width: '20rem'}}>
                    <div className="card-body">
                      <h5 className="card-title">{manufacturer.name}</h5>
                    </div>
                  </div>
                </tr>
              )
            })}
          </tbody>
        </>
    )
}

export default ManufacturersList
