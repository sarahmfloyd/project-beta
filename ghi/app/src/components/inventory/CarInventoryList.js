import React, { useEffect, useState } from 'react';
import './inventory.css'

function AutomobilesList () {
    const [cars, setCars] = useState([])

    const getData = async () => {
        const response = await fetch("http://localhost:8090/api/sales/cars/")
        if (response.ok) {
            const data = await response.json()
            setCars(data.cars)
        }
    }
    useEffect(() =>{
        getData()
    }, [])

    return (
      <>
      <div className='main-inv' id="picture">
        {/* <img id="picture" src={'https://images.fineartamerica.com/images/artworkimages/mediumlarge/2/dodge-challenger-classic-car-black-white-hotte-hue.jpg'}/> */}
      </div>
        <div class="mask rgba-black-light align-items-center"></div>
          <div class="container">
            <div class="row">
              <div class="col-md-12 mb-4 white-text text-center">
              <h1 class="h1-reponsive white-text text-uppercase font-weight-bold mb-0 pt-md-5 pt-5 wow fadeInDown" data-wow-delay="0.3s"><strong>Current Inventory</strong></h1>
                <table className="table table-striped">
                  <thead>
                    <tr>
                      <th>Vin</th>
                      <th>Model</th>
                      <th>Color</th>
                    </tr>
                  </thead>
                  <tbody>
                    {cars.map(car => {
                      return (
                        <tr key={car.href}>
                          <td>{car.vin}</td>
                          <td>{car.model}</td>
                          <td>{car.color}</td>
                        </tr>
                        )
                      })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
      </>
    )
}

export default AutomobilesList
