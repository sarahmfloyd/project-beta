import React, { useEffect, useState } from 'react';

function VehicleModelList () {
    const [cars, setCars] = useState([])

    const getData = async () => {
        const response = await fetch("http://localhost:8100/api/models/")
        if (response.ok) {
            const data = await response.json()
            setCars(data.models)
        }
    }

    useEffect(() =>{
        getData()
    }, [])

    return (
      <>
        <h1> Vehicle Models </h1>
        <hr className="my-5" />
        <div className="row row-cols-1 row-cols-md-2 g-4">
          {cars.map(car => {
          return (
            <div key={car.href}>
              <div className="col">
                <div className="card text-white bg-dark mb-3" style={{ width: '30rem'}}>
                  <img className="img-thumbnail" src={car.picture_url} alt=""></img>
                  <div className="card-body">
                    <h5 className="card-title">{car.manufacturer.name}</h5>
                  </div>
                    <ul className="list-group list-group-flush">
                      <li className="list-group-item">{car.name}</li>
                    </ul>
                </div>
              </div>

            </div>
            )
        })}
        </div>
    </>
  )
}

export default VehicleModelList
