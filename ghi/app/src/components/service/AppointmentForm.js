import React, { useState, useEffect } from "react";

function AppointmentForm() {
  const [technician, setTechnician] = useState([]);

  const [formData, setFormData] = useState({
    vin: "",
    customer: "",
    date: "",
    time: "",
    technician: "",
    reason: "",
  });

  const getData = async () => {
    const technicianUrl = "http://localhost:8080/api/technicians/";
    const response = await fetch(technicianUrl);
    if (response.ok) {
      const data = await response.json();
      setTechnician(data.technician);
    }
  };
  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = "http://localhost:8080/api/appointments/";

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        vin: "",
        customer: "",
        date: "",
        time: "",
        technician: "",
        reason: "",
      });
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create an appointment</h1>
          <form onSubmit={handleSubmit} id="create-appointment-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.vin}
                placeholder="VIN"
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
              />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.customer}
                placeholder="Customer Name"
                required
                type="text"
                name="customer"
                id="customer"
                className="form-control"
              />
              <label htmlFor="customer">Customer Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.date}
                placeholder="Date"
                required
                type="text"
                name="date"
                id="date"
                className="form-control"
              />
              <label htmlFor="date">Date</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.time}
                placeholder="Time"
                required
                type="text"
                name="time"
                id="time"
                className="form-control"
              />
              <label htmlFor="time">Time</label>
            </div>
            <div className="form-floating mb-3">
              <select
                onChange={handleFormChange}
                value={formData.technician}
                placeholder="Techncian"
                name="technician"
                id="technician"
                className="form-select"
              >
                <option value="">Technicians</option>
                {technician.map((technician) => {
                  return (
                    <option key={technician.id} value={technician.id}>
                      {technician.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.reason}
                placeholder="Reason"
                required
                type="text"
                name="reason"
                id="reason"
                className="form-control"
              />
              <label htmlFor="reason">Reason</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AppointmentForm;
