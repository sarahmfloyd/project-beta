import React, { useEffect, useState } from 'react';

function SalesRecordList () {
    const [sales, setSales] = useState([])

    const getData = async () => {
        const response = await fetch("http://localhost:8090/api/sales/")
        if (response.ok) {
            const data = await response.json()
            setSales(data.sales_record)
        }
    }

    useEffect(() =>{
        getData()
    }, [])

    return (
      <>
        <h1> Car Sales History </h1>
        <table className="table table-striped">
            <thead>
            <tr>
              <th>Salesperson</th>
              <th>Employee number</th>
              <th>Customer name</th>
              <th>Vin</th>
              <th>Sales price</th>
            </tr>
          </thead>
          <tbody>
            {sales.map(sale => {
              return (
                <tr key={sale.href}>
                    <td>{sale.sales_person.name}</td>
                    <td>{sale.sales_person.employee_number }</td>
                    <td>{sale.customer.name}</td>
                    <td>{sale.automobile.vin}</td>
                    <td>{sale.price}</td>
                </tr>
              )
            })}
          </tbody>
        </table>
        </>
    )
}

export default SalesRecordList
