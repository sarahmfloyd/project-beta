import React, {useEffect, useState} from 'react';

function SalesPersonRecordList() {
    const [filterValue, setFilterValue] = useState("");
    const [sales, setSales] = useState([]);

    const getData = async () => {
        const url = 'http://localhost:8090/api/sales/'
        const response = await fetch(url)
        console.log(response)
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales_record);
        }
    }
    useEffect (() => {
        getData()
    }, [])

    const handleChange = (event) => {
        setFilterValue(event.target.value);
    }

    const filteredPerson = () => {
        return sales.filter((sale) =>
            sale.sales_person.name.toLowerCase().includes(filterValue)
            )
    }

    return (
        <>
        <h1> Salesperson Sales History </h1>
        <input onChange={handleChange} placeholder="Search name" />
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Salesperson</th>
                    <th>Employee number</th>
                    <th>Customer name</th>
                    <th>Vin</th>
                    <th>Sales price</th>
                </tr>
                </thead>
                <tbody>
                    {filteredPerson().map((sale) => {
                        return (
                        <tr>
                            <td>{sale.sales_person.name }</td>
                            <td>{sale.sales_person.employee_number }</td>
                            <td>{sale.customer.name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.price}</td>
                        </tr>
                    )
                })}
                </tbody>
            </table>
        </>
    )
}
export default SalesPersonRecordList;
