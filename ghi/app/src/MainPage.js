import { NavLink } from "react-router-dom";
import "./index.css";

function MainPage() {
  return (
    <>
      <div id="intro" className="container">
        <img
          src={
            "https://media.autoexpress.co.uk/image/private/s--X-WVjvBW--/f_auto,t_content-image-full-desktop@1/v1562242672/autoexpress/2016/05/1968-chevrolet-corvette-1.jpg"
          }
        />

        <div
          className="px-5 py-2 my-5 text-light position-absolute"
          id="main-view"
        >
          <h1 className="display-1 fw-bold text-center"></h1>
          <div className="mask rgba-black-light align-items-center">
            <div className="container">
              <div className="row">
                <div className="col-md-12 mb-4 white-text text-center">
                  <h1
                    className="h1-reponsive white-text text-uppercase font-weight-bold mb-0 pt-md-5 pt-5 wow fadeInDown"
                    data-wow-delay="0.3s"
                  >
                    <strong>Shift into Gear</strong>
                  </h1>
                  <div className="text-center p-2">
                    <NavLink
                      to="/inventory/automobiles"
                      className="btn btn-outline-white text-white wow fadeInDown"
                    >
                      View Inventory
                    </NavLink>
                    <NavLink
                      to="/appointments/new"
                      className="btn btn-outline-white text-white wow fadeInDown"
                    >
                      Make Appointment
                    </NavLink>
                  </div>
                  <hr
                    class="hr-light my-4 wow fadeInDown"
                    data-wow-delay="0.4s"
                  ></hr>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <br></br>
      <hr class="hr-light my-4 wow fadeInDown" data-wow-delay="0.4s"></hr>
      <main>
        <div className="container">
          <div className="col-md-12 text-dark text-center">
            <p>
              American muscle cars are high-performance vehicles that were
              popular in the United States during the 1960s and 1970s. They are
              characterized by their powerful engines, aggressive design, and
              rear-wheel drive. Examples of popular muscle cars include the
              Chevrolet Camaro, Ford Mustang, and Dodge Challenger. These
              vehicles were often used for drag racing and street racing and
              were considered symbols of American power and speed. Despite
              declining sales in the 1970s and 1980s, muscle cars have seen a
              resurgence in popularity in recent years, with new models being
              produced by several car manufacturers. Muscle cars remain an
              iconic symbol of American automobile culture and remain highly
              prized by collectors and enthusiasts.
            </p>
          </div>
        </div>
      </main>
    </>
  );
}

export default MainPage;
