# CarCar

Team:

* Sarah Floyd - Sales
* Scott Bustos - Service

## Design
Bootstrap
## Service microservice
For my models I used a AutomobileVO that polled data from the inventory service Automobile model. The data that was returned was the vin number. I created an appointment model that also had the vin number as well to allow for the company to check if the vin numbers matched in the system. The appointment model also included the customer(this would allow them to enter their name),
the date, time, reason, vip(If the vin numbers matched in the system then you would be a vip customer,) and  made technician a foreign key to appointment because every appointment needs a technician. For my technician model I used the technician name and their employee number.

## Sales microservice
Models: For the SalesPerson and PotentialCustomer models, I used the variable that were given in Learn to specify what each model should have. For the SalesRecord model, I used 3 foreign keys to connect with the previous models attributes because I knew that they were needed to create a sales record and the information was dependent on what was linked to those models. Similarly, I created a value object (AutoDetailVO) that used the same attributes as the Automobile model in inventory.
Poller: The url I used to poll for was inventory api for automobiles. I used a for-loop to loop through my AutoDetailVO to ensure than it was polling the vin, model name and color of the automobile. 
