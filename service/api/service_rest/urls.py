from django.urls import path
from .views import list_appointments, create_technician, show_appointment


urlpatterns = [
    path("appointments/", list_appointments, name="list_appointments"),
    path("technicians/", create_technician, name="create_technicians"),
    path('appointments/<int:id>', show_appointment, name='show_appointment'),
]
